<?php

namespace Viscomp\ClickSSL\Client;

use Application\ClickSSL\Objects\AuthUser;
use Zend\Soap\Client\DotNet;

class Client
{
    /**
     * @var AuthUser
     */
    private $authUser;

    private $soapClient;

    public function __construct(AuthUser $authUser, DotNet $dotNet)
    {
        $this->authUser = $authUser;
        $this->soapClient = $dotNet;
    }

    public function setSoapClient($client)
    {
        $this->soapClient->setSoapClient($client);
    }

    public function getSoapClient()
    {
        return $this->soapClient;
    }

    public function call($method, $params = [])
    {
        return $this->soapClient->call($method, $params);
    }
}
