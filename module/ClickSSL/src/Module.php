<?php

namespace Viscomp\ClickSSL;

use Viscomp\ClickSSL\Client\Client;

class Module
{
    public function getConfig()
    {
        $provider = new ConfigProvider();
        return [
            'service_manager' => $provider->getDependencyConfig(),
        ];
    }
}
