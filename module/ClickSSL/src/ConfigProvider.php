<?php

namespace Viscomp\ClickSSL;

use Viscomp\ClickSSL\Client\Client;
use Viscomp\ClickSSL\Client\ClientFactory;

class ConfigProvider
{
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencyConfig(),
        ];
    }

    public function getDependencyConfig()
    {
        return [
            'factories' => [
                Client::class => ClientFactory::class,
            ],
        ];
    }
}
