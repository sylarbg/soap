<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\ClickSSL\Client\Client;
use Application\ClickSSL\Client\ClientFactory;
use Application\ClickSSL\Objects\Error;
use Application\ClickSSL\Objects\GetProductPriceResponse;
use Application\ClickSSL\Objects\ProductPriceResponse;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'click_ssl' => [
        'credentials' => [
            'email' => 'g.grozdev@euroweb.de',
            'password' => 'ni06UiydWTG3k'
        ],
        'wsdl' => 'https://test-api.clickssl.com/quickorderapi.asmx?WSDL',
        'client_soap_options' => [
                'classmap' => [
                    'ProductPriceResponse' => ProductPriceResponse::class,
                   //  'GetProductPriceResponse' => GetProductPriceResponse::class,
                    //'ValidateOrderWithCSRResponse' => Book::class,
                    'Error' => Error::class,
                    //   'APIError' => ApiProblem::class,
                ]
        ],
    ],
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'application' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/application[/:action]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
           Client::class => ClientFactory::class,
        ],
    ],

    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
