<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ApplicationTest\Controller;

use Application\ClickSSL\ApiProblem;
use Application\ClickSSL\Client\Client;
use Application\ClickSSL\Client\ClientFactory;
use Application\ClickSSL\Objects\AuthUser;
use Application\ClickSSL\Objects\Error;
use Application\ClickSSL\Objects\GetProductPriceResponse;
use Application\ClickSSL\Objects\ProductPriceRequest;
use Application\ClickSSL\Objects\ProductPriceResponse;
use Application\ClickSSL\Objects\GetProductPrice;
use Application\Fake;
use Zend\Hydrator\ObjectProperty;

use Zend\Soap\Client\DotNet;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class IndexControllerTest extends AbstractHttpControllerTestCase
{
    /**
     * @var DotNet
     */
    private $client;
    private $responses;

    public function setUp()
    {
        $configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            $configOverrides
        ));

        $this->client = new DotNet(
            'https://test-api.clickssl.com/quickorderapi.asmx?WSDL',
            array(
                'classmap' => array(
                    'ProductPriceResponse' => ProductPriceResponse::class,                   
                    'Error' => Error::class,
                )
            )
        );

        $this->responses = include  __DIR__.'/../c.php';

        parent::setUp();
    }

    public function fakeSoapClient(Client $client, $responseKey)
    {
        if (!array_key_exists($responseKey, $this->responses)) {
            throw new \RuntimeException("Missing response key ". $responseKey);
        }

        return Fake::make('https://test-api.clickssl.com/quickorderapi.asmx?WSDL', [
            'exceptions' => true,
            'classmap' => $client->getSoapClient()->getClassmap(),
        ], $this->responses[$responseKey]);
    }

    public function testIndexActionCanBeAccessed()
    {
        /** @var Client $client */
        $client = $this->getApplicationServiceLocator()->get(Client::class);

        $fakeSoapClient = $this->fakeSoapClient($client, 'GetProductPriceSuccess');

        $client->setSoapClient($fakeSoapClient);
        $objRequest = new GetProductPrice();
        $h          = new ObjectProperty();
        $h->hydrate([
            'AuthUser' => [
                'PartnerEmail' => 'test',
                'Password' => 'test',
            ],
            'Year' => 1,
            'ProductID' => 1,
        ], $objRequest);

        $r = $client->call('GetProductPrice', [['productPriceRequest' => $objRequest]]);

        var_dump($r);
    }
}
