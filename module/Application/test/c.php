<?php

return [
    'GetProductPriceSuccess' => '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
        <GetProductPriceResponse xmlns="http://tempuri.org/">
            <GetProductPriceResult>
                <ProductName>Product 1</ProductName>
                <Year>1</Year>
                <Price>13.9</Price>
                <AddDomainPrice>0</AddDomainPrice>
                <error>
                    <ErrorCode>1</ErrorCode>
                    <ErrorField></ErrorField>
                    <ErrorMessage></ErrorMessage>
                </error>
            </GetProductPriceResult>
        </GetProductPriceResponse>
    </soap:Body>
</soap:Envelope>',
    'ValidateOrderWithCSR' => '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <ValidateOrderWithCSRResponse xmlns="http://tempuri.org/">
      <ValidateOrderWithCSRResult>
        <ErrorCode>0</ErrorCode>
        <ErrorField></ErrorField>
        <ErrorMessage></ErrorMessage>
      </ValidateOrderWithCSRResult>
    </ValidateOrderWithCSRResponse>
  </soap:Body>
</soap:Envelope>'
];
