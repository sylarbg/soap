<?php

namespace Application\ClickSSL\Objects;

class ProductPriceRequest
{

    public function __construct($authUser, $productID, $year)
    {
        $this->AuthUser = $authUser;
        $this->ProductID = $productID;
        $this->Year = $year;
    }

    /**
     * @var AuthUser
     */
    private $AuthUser = null;

    /**
     * @var int
     */
    private $ProductID = null;

    /**
     * @var int
     */
    private $Year = null;

    /**
     * @return AuthUser
     */
    public function getAuthUser()
    {
        return $this->AuthUser;
    }

    /**
     * @return int
     */
    public function getProductID()
    {
        return $this->ProductID;
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->Year;
    }


}

