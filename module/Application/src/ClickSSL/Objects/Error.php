<?php

namespace Application\ClickSSL\Objects;

class Error
{

    /**
     * @var int
     */
    private $ErrorCode = null;

    /**
     * @var string
     */
    private $ErrorField = null;

    /**
     * @var string
     */
    private $ErroMessage = null;

    /**
     * @var string
     */
    private $ErrorMessage = null;

    /**
     * @return int
     */
    public function getErrorCode()
    {
        return $this->ErrorCode;
    }

    /**
     * @return string
     */
    public function getErrorField()
    {
        return $this->ErrorField;
    }

    /**
     * @return string
     */
    public function getErroMessage()
    {
        return $this->ErroMessage;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->ErrorMessage;
    }


}

