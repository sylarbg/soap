<?php

namespace Application\ClickSSL\Objects;

class AuthUser
{
    public function __construct($email, $password)
    {
        $this->PartnerEmail = $email;
        $this->Password = $password;
    }

    /**
     * @var string
     */
    private $PartnerEmail = null;

    /**
     * @var string
     */
    private $Password = null;

    /**
     * @return string
     */
    public function getPartnerEmail()
    {
        return $this->PartnerEmail;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->Password;
    }
}

