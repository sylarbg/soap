<?php

namespace Application\ClickSSL\Objects;

class ProductPriceResponse
{

    /**
     * @var string
     */
    public $ProductName = null;

    /**
     * @var int
     */
    private $Year = null;

    /**
     * @var float
     */
    private $Price = null;

    /**
     * @var float
     */
    private $AddDomainPrice = null;

    /**
     * @var Error
     */
    private $error = null;

    /**
     * @return string
     */
    public function getProductName()
    {
        return $this->ProductName;
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->Year;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->Price;
    }

    /**
     * @return float
     */
    public function getAddDomainPrice()
    {
        return $this->AddDomainPrice;
    }

    /**
     * @return Error
     */
    public function getError()
    {
        return $this->error;
    }
}
