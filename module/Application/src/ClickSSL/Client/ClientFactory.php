<?php
namespace Application\ClickSSL\Client;

use Application\ClickSSL\Objects\AuthUser;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Soap\Client\DotNet;

class ClientFactory implements FactoryInterface
{
    const CLICK_SSL_CONFIG_KEY = 'click_ssl';
    private $config;
    private $container;

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $this->container = $container;
        return new Client($this->credentials(), $this->soapClient());
    }

    private function soapClient()
    {
        $config = $this->config();
        if (!array_key_exists('wsdl', $config)) {
            throw new \Exception("Missing wsdl key");
        }

        if (!array_key_exists('client_soap_options', $config)) {
            throw new \Exception("Missing client_soap_options key");
        }

        return new DotNet($config['wsdl'], $config['client_soap_options']);
    }

    private function config()
    {
        if (!$this->config) {
            $config = $this->container->get('Config');
            if (!isset($config[static::CLICK_SSL_CONFIG_KEY])) {
                $config[static::CLICK_SSL_CONFIG_KEY] = [];
            }
            $this->config = $config[self::CLICK_SSL_CONFIG_KEY];
        }
        return $this->config;
    }

    private function credentials()
    {
        $config = $this->config();
        if (!array_key_exists('credentials', $config)) {
            throw new \Exception("Missing credentials key");
        }
        $credentials = $config['credentials'];
        if (!array_key_exists('email', $credentials) ||
            !array_key_exists('password', $credentials)) {
            throw new \Exception("Missing credentials");
        }
        return new AuthUser($credentials['email'], $credentials['password']);
    }
}
