<?php
/**
 * Created by PhpStorm.
 * User: sylar
 * Date: 13.07.17
 * Time: 00:26
 */
namespace Application;

use Zend\Soap\Client\Common;

class Fake
{
    public static function callBack($response)
    {
        return function(Common $client, $request, $location, $action, $version, $oneWay = null) use ($response) {
            return $response;
        };
    }

    public static function make($wdls, $options, $response)
    {
        return new Common(
            static::callBack($response),
            $wdls,
            $options
        );
    }


}